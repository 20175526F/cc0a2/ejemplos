package com.example.recordar;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class GridAdapter extends BaseAdapter {

    Context context;
    List<String> names;
    List<Integer> images;
    List<String> descriptions;

    public GridAdapter(Context context, List<String> names, List<Integer> images, List<String> descriptions) {
        this.context = context;
        this.names = names;
        this.images = images;
        this.descriptions = descriptions;
    }

    @Override
    public int getCount() {
        return names.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        @SuppressLint("ViewHolder") View view_local = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.matrix,viewGroup,false);
        ImageView imageView = view_local.findViewById(R.id.image_view);
        TextView textView = view_local.findViewById(R.id.tv_plato);
        TextView textView2 = view_local.findViewById(R.id.tv_descripcion);
        imageView.setImageResource(images.get(i));
        textView.setText(names.get(i));
        textView2.setText(descriptions.get(i));
        return view_local;
    }
}
