package com.example.recordar;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    GridView gridView;
    List<String> names = new ArrayList<>();
    List<Integer> images = new ArrayList<>();
    List<String> descriptions = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        gridView = findViewById(R.id.grid_view);

        fillArray();

        GridAdapter adapter = new GridAdapter(this,names,images, descriptions);
        gridView.setAdapter(adapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(getApplicationContext(),names.get(i),Toast.LENGTH_SHORT).show();
                /*Intent intent = new Intent(MainActivity.this, Activity2.class);
                String title = names.get(i).toString();
                intent.putExtra("TEXT", title);
                startActivity(intent);*/
            }
        });



    }

    private void fillArray(){
        names.add("Ceviche de Pescado");
        names.add("Pollo a la Brasa");
        names.add("Chicharron de chancho");
        names.add("Aji de Gallina");
        names.add("Pescado frito");
        names.add("Arroz con pollo");
        names.add("Milanesa de pollo");
        names.add("Tallarines con pollo");

        images.add(R.drawable.ceviche);
        images.add(R.drawable.pollo);
        images.add(R.drawable.chicharron);
        images.add(R.drawable.ajigallina);
        images.add(R.drawable.pescado);
        images.add(R.drawable.arrozpollo);
        images.add(R.drawable.milanesa);
        images.add(R.drawable.tallarines);

        descriptions.add("Picante y sin picar");
        descriptions.add("Papas crocantes");
        descriptions.add("Desde Lurin");
        descriptions.add("Con la receta secreta");
        descriptions.add("Directo del mar");
        descriptions.add("Con huancaina");
        descriptions.add("Con papas fritas");
        descriptions.add("Rojos con pollo");
    }
}

