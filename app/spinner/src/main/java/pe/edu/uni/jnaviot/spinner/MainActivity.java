package pe.edu.uni.jnaviot.spinner;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;

public class MainActivity extends AppCompatActivity {

    ImageView imageView;
    Spinner spinner;

    ArrayAdapter<CharSequence> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView = findViewById(R.id.image_view);
        spinner = findViewById(R.id.spinner);

        //adapter = ArrayAdapter.createFromResource(this, R.array.string_array, android.R.layout.simple_spinner_item);
        adapter = ArrayAdapter.createFromResource(this, R.array.string_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(i==0){
                    imageView.setImageResource(R.drawable.lotus1);
                    return;
                }
                if(i==1){
                    imageView.setImageResource(R.drawable.lotus2);
                    return;
                }
                if(i==2){
                    imageView.setImageResource(R.drawable.rueda);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });



    }
}