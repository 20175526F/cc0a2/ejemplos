package pe.edu.uni.jnaviot.gameapp;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.Random;

public class PlayGameActivity extends AppCompatActivity {

    TextView attempt, remain, hint;
    EditText textGuessNumber;
    Button buttonPlay;
    boolean twoDigits, threeDigits, fourDigits;
    Random r = new Random();
    int remainingLife = 10;
    int randomNumber;

    ArrayList<Integer> list_attempts = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_game);


        attempt = findViewById(R.id.text_view_last);
        remain = findViewById(R.id.text_view_remain);
        hint = findViewById(R.id.text_view_hint);
        buttonPlay = findViewById(R.id.button_pg);

        textGuessNumber = findViewById(R.id.edit_text_pg);

        Intent intent = getIntent();

        twoDigits = intent.getBooleanExtra(String.valueOf(R.string.key_two), false);
        threeDigits = intent.getBooleanExtra(String.valueOf(R.string.key_three), false);
        fourDigits = intent.getBooleanExtra(String.valueOf(R.string.key_four), false);

        if(twoDigits){
            randomNumber = r.nextInt(90)+10;
        }
        if(threeDigits){
            randomNumber = r.nextInt(900)+100;
        }
        if(fourDigits){
            randomNumber = r.nextInt(9000)+1000;
        }

        buttonPlay.setOnClickListener(view -> {

            String guessString = textGuessNumber.getText().toString();
            remainingLife--;

            if(guessString.equals("")){
                Snackbar.make(view, "Ingrese un numero", Snackbar.LENGTH_LONG).show();
            }else{
                int guessInt = Integer.parseInt(guessString);
                list_attempts.add(guessInt);
                Resources res = getResources();
                attempt.setText(String.format(res.getString(R.string.text_view_last_attempt),guessInt));
                remain.setText(String.format(res.getString(R.string.text_view_last_remain), remainingLife));

                if(guessInt == randomNumber){
                    AlertDialog.Builder builder  = new AlertDialog.Builder(PlayGameActivity.this);
                    builder.setTitle(R.string.dialog_title);
                    builder.setCancelable(false);
                    builder.setMessage(String.format(res.getString(R.string.game_msg_win),randomNumber,
                            (10-remainingLife), list_attempts.toString() ));
                    builder.setPositiveButton("Si", (dialogInterface, i) -> {
                        Intent intent1 = new Intent(PlayGameActivity.this, GameAppActivity.class);
                        startActivity(intent1);
                        finish();
                    });
                    builder.setNegativeButton("No", (dialogInterface, i) -> {
                        moveTaskToBack(true);
                        android.os.Process.killProcess(android.os.Process.myPid());
                        System.exit(1);
                    });
                    builder.create().show();

                }
                if(guessInt < randomNumber){

                    hint.setText(R.string.text_view_hint_sube);
                }
                if(guessInt > randomNumber){
                    hint.setText(R.string.text_view_hint_baja);
                }
                if(remainingLife == 0 ) {
                    AlertDialog.Builder builder  = new AlertDialog.Builder(PlayGameActivity.this);
                    builder.setTitle(R.string.dialog_title);
                    builder.setCancelable(false);
                    builder.setMessage(String.format(res.getString(R.string.game_msg_lose),randomNumber,
                            list_attempts.toString() ));
                    builder.setPositiveButton("Si", (dialogInterface, i) -> {
                        Intent intent1 = new Intent(PlayGameActivity.this, GameAppActivity.class);
                        startActivity(intent1);
                        finish();
                    });
                    builder.setNegativeButton("No", (dialogInterface, i) -> {
                        moveTaskToBack(true);
                        android.os.Process.killProcess(android.os.Process.myPid());
                        System.exit(1);
                    });
                    builder.create().show();

                }
                textGuessNumber.setText("");
            }
        });



    }
}