package pe.edu.uni.jnaviot.gameapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.RadioButton;

import com.google.android.material.snackbar.Snackbar;

public class GameAppActivity extends AppCompatActivity {
    RadioButton radioButton1, radioButton2, radioButton3;
    Button button1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_app);

        radioButton1 = findViewById(R.id.rb1);
        radioButton2 = findViewById(R.id.rb2);
        radioButton3 = findViewById(R.id.rb3);

        button1 = findViewById(R.id.button_mg);

        button1.setOnClickListener(view -> {
            Intent intent =  new Intent(GameAppActivity.this, PlayGameActivity.class);
            if(!(radioButton1.isChecked() || radioButton2.isChecked() || radioButton3.isChecked())){
                Snackbar.make(view, R.string.snackbar_mg,Snackbar.LENGTH_LONG).show();
            }else{
                if(radioButton1.isChecked()){
                    intent.putExtra(String.valueOf(R.string.key_two),true);
                }
                if(radioButton2.isChecked()){
                    intent.putExtra(String.valueOf(R.string.key_three),true);
                }
                if(radioButton3.isChecked()){
                    intent.putExtra(String.valueOf(R.string.key_four),true);
                }
                startActivity(intent);

            }


        });

    }
}