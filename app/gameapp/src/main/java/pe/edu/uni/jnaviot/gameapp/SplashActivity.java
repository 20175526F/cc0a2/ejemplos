package pe.edu.uni.jnaviot.gameapp;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

@SuppressLint("CustomSplashScreen")
public class SplashActivity extends AppCompatActivity {

    ImageView imageView;
    TextView textView;
    Animation animationImage, animationText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        imageView = findViewById(R.id.image_view_1);
        textView = findViewById(R.id.text_1);

        animationImage = AnimationUtils.loadAnimation(this, R.anim.image_rotation);
        animationText = AnimationUtils.loadAnimation(this, R.anim.text_anim);

        imageView.setAnimation(animationImage);
        textView.setAnimation(animationText);

        new CountDownTimer(5000, 1000) {
            @Override
            public void onTick(long l) {
            }
            @Override
            public void onFinish() {
                Intent intent = new Intent(SplashActivity.this, GameAppActivity.class);
                startActivity(intent);
                finish();
            }
        }.start();



    }
}