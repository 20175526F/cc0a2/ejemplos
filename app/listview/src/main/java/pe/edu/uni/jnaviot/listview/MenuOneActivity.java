package pe.edu.uni.jnaviot.listview;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MenuOneActivity extends AppCompatActivity {

    Button button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_one);

        button = findViewById(R.id.button1);

        Intent intent = getIntent();
        String text = intent.getStringExtra("TEXT");

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), String.format(getResources().getString(R.string.toast_two),text), Toast.LENGTH_LONG).show();
            }
        });

    }
}