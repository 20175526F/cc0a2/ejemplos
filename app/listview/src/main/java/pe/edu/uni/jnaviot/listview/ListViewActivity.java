package pe.edu.uni.jnaviot.listview;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class ListViewActivity extends AppCompatActivity {

    ListView listView;
    String[] countries;

    ArrayAdapter<String>arrayAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view);

        listView=findViewById(R.id.list_view);
        countries=getResources().getStringArray(R.array.countries);

        arrayAdapter=new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1,
                countries);

        listView.setAdapter(arrayAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //consigue elemento de posicion i y lo coloca en el adapterView para sacar string
                String country = adapterView.getItemAtPosition(i).toString();
                Toast.makeText(getApplicationContext(),String.format(getResources().getString(R.string.toast_msg),country),Toast.LENGTH_LONG).show();

                Intent intent = new Intent(ListViewActivity.this, MenuOneActivity.class);
                intent.putExtra("TEXT", country);
                startActivity(intent);
            }
        });

    }
}